# Rust Sokoban

Based on [https://sokoban.iolivia.me/](https://sokoban.iolivia.me/)

## Dependencies
On linux libudev and libalsasound2 likely need to be installed:

(On Debian/Ubuntu) `sudo apt-get install libudev-dev libasound2-dev`

## Running
* `cargo run` does a dev build and runs
* `cargo run --release` does a release build and runs