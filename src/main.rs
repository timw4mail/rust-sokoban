use ggez;
use ggez::event::KeyCode;
use ggez::event::KeyMods;
use ggez::{conf, event, timer, Context, GameResult};
use specs::{RunNow, World, WorldExt};

use std::fs::File;
use std::io::prelude::*;
use std::path;

mod audio;
mod components;
mod constants;
mod entities;
mod events;
mod map;
mod resources;
mod systems;

use crate::audio::initialize_sounds;
use crate::components::*;
use crate::map::*;
use crate::resources::*;
use crate::systems::*;

// All the game state
struct Game {
    world: World,
}

impl event::EventHandler for Game {
    fn update(&mut self, context: &mut Context) -> GameResult {
        // Run input system
        {
            let mut is = InputSystem {};
            is.run_now(&self.world);
        }

        // Run gameplay state system
        {
            let mut gss = GameplayStateSystem {};
            gss.run_now(&self.world);
        }

        // Get an update time resource
        {
            let mut time = self.world.write_resource::<Time>();
            time.delta += timer::delta(context);
        }

        // Run event system
        {
            let mut es = EventSystem {};
            es.run_now(&self.world);
        }

        Ok(())
    }

    fn draw(&mut self, context: &mut Context) -> GameResult {
        // Render game entities
        {
            let mut rs = RenderingSystem { context };
            rs.run_now(&self.world);
        }

        Ok(())
    }

    fn key_down_event(
        &mut self,
        context: &mut Context,
        keycode: KeyCode,
        keymod: KeyMods,
        _repeat: bool,
    ) {
        // println!("Key pressed: {:?}, {:?}", keycode, _keymod);

        match (keycode, keymod) {
            // Map common keys to quit
            (KeyCode::Escape, KeyMods::NONE)
            | (KeyCode::Q, KeyMods::CTRL)
            | (KeyCode::Q, KeyMods::LOGO) => ggez::event::quit(context),
            _ => {
                let mut input_queue = self.world.write_resource::<InputQueue>();
                input_queue.keys_pressed.push(keycode);
            }
        }
    }
}

pub fn initialize_level(world: &mut World, level: u32) {
    let map_file = format!("resources/maps/{:02}.txt", level);

    let mut file = File::open(&map_file).expect("Failed to open map file");
    let mut map = String::new();
    file.read_to_string(&mut map)
        .expect("failed to read map file");

    load_map(world, map);
}

pub fn main() -> GameResult {
    let mut world = World::new();
    register_components(&mut world);
    register_resources(&mut world);

    // @TODO multiple levels
    initialize_level(&mut world, 1);

    // Create a game context and event loop
    let context_builder = ggez::ContextBuilder::new("rust_sokoban", "Timothy J. Warren")
        .window_setup(conf::WindowSetup::default().title("Rust Sokoban!"))
        .window_mode(conf::WindowMode::default().dimensions(800.0, 600.0))
        .add_resource_path(path::PathBuf::from("./resources"));

    let (context, event_loop) = &mut context_builder.build()?;
    initialize_sounds(&mut world, context);

    // Create the game state
    let game = &mut Game { world };

    // Run the main event loop
    event::run(context, event_loop, game)
}
