use crate::components::{BoxColor, Position};
use crate::entities::*;
use specs::World;

pub fn load_map(world: &mut World, map_string: String) {
    // read all lines
    let rows: Vec<&str> = map_string.trim().split('\n').map(|x| x.trim()).collect();

    for (y, row) in rows.iter().enumerate() {
        let columns: Vec<&str> = row.split(' ').collect();

        for (x, column) in columns.iter().enumerate() {
            // Create the position at which to create something on the map
            let position = Position::new(x as u8, y as u8);

            // Figure out which object to create
            match *column {
                "." => create_floor(world, position),
                "X" => {
                    create_floor(world, position);
                    create_wall(world, position);
                }
                "@" => {
                    create_floor(world, position);
                    create_player(world, position);
                }
                "*" => {
                    create_floor(world, position);
                    create_box(world, position, BoxColor::Brown);
                }
                "+" => {
                    create_floor(world, position);
                    create_box(world, position, BoxColor::Red);
                }
                "%" => {
                    create_floor(world, position);
                    create_box_spot(world, position, BoxColor::Brown);
                }
                "=" => {
                    create_floor(world, position);
                    create_box_spot(world, position, BoxColor::Red);
                }
                "-" => (),
                c => panic!("unrecognized map item {}", c),
            }
        }
    }
}
